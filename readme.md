# HalloWorld-OS
--- 
## Console
### Commands

> ### cls  
>     
> **Funcs:**  
> - This command can be used to clear console 

> ### cd *{ path }*  
> **Params:**  
> - **path** - you can specify full path or just one dir
>     
> **Funcs:**  
> - This command can be used to change directory  

> ### md *{ path }*  
> **Params:**  
> - **path** - you can specify full path or just one dir  
> 
> **Funcs:**  
> - This command can be used to crete directory

> ### mf *{ path }*  
> **Params:**  
> - **path** - you can specify full path or just one file  
> 
> **Funcs:**  
> - This command can be used to crete any file

> ### ld *{ path }*  
> **Params:**  
> - **path** - you can specify full path or just one dir or nothing
> 
> **Funcs:**  
> - This command can be used to list out files/dirs

> ### contentof *{ path }*
> **Params:**  
> - **path** - you can specify full path or just one file 
> 
> **Funcs:**  
> - This command can be used to see what is inside file

> ### rd *{ path param1 }*
> **Params:**  
> - **path** - you can specify full path or just one file
> - **param1**
>   - *r* - remove dir with all its content 
>   - *none(default)* - remove only empty dir
> 
> **Funcs:**  
> - This command can be used to delete dir

> ### rf *{ path }*
> **Params:**  
> - **path** - you can specify full path or just one file
> 
> **Funcs:**  
> - This command can be used to delete file

> ### open *{ path }*
> **Params:**  
> - **path** - you can specify full path or just one file
> 
> **Funcs:**  
> - This command can be used to open file in gui text editor (for now you cant edit/save, just read)

> ### exit
> 
> **Funcs:**  
> - This command can be used to exit program